/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.core;

import static com.ail.core.Functions.isEmpty;
import static javax.ws.rs.HttpMethod.GET;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.lang.reflect.Method;
import java.util.Map;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientRequestFactory;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.InvokeRestfulEndpointService.InvokeRestfulEndpointArgument;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;

@ServiceImplementation
public class InvokeRestfulEndpointService extends Service<InvokeRestfulEndpointArgument> {

    @Override
    public void invoke() throws BaseException {
        try {
            ClientRequest request = new ClientRequestFactory().createRequest(args.getUrlArg());

            request = setHeaders(request);

            request = setQueryParameters(request);

            request = setBody(request);

            Method requestMethod = getRequestMethod(request);

            Object response = requestMethod.invoke(request);

            args.setResponseRet(response);
        } catch (Exception e) {
            throw new PostconditionException(e.getMessage(), e);
        }
    }

    public ClientRequest setHeaders(ClientRequest request) {
        if (args.getHeadersArg() != null) {
            for (String headerName : args.getHeadersArg().keySet()) {
                if (isEmpty(args.getAuthorizationTokenArg()) || !AUTHORIZATION.equals(headerName)) {
                    // don't add the auth header if it's specified separately
                    request = request.header(headerName, args.getHeadersArg().get(headerName));
                }
            }
        }

        if (!isEmpty(args.getAuthorizationTokenArg())) {
            request = request.header(AUTHORIZATION, args.getAuthorizationTokenArg());
        }

        return request;
    }

    public ClientRequest setQueryParameters(ClientRequest request) {
        if (args.getQueryParametersArg() != null) {
            for (String queryParameterName : args.getQueryParametersArg().keySet()) {
                request = request.queryParameter(queryParameterName, args.getQueryParametersArg().get(queryParameterName));
            }
        }

        return request;
    }

    public ClientRequest setBody(ClientRequest request) {
        if (!isEmpty(args.getBodyArg())) {
            String bodyContentType = APPLICATION_JSON;
            if (!isEmpty(args.getBodyContentTypeArg())) {
                bodyContentType = args.getBodyContentTypeArg();
            }
            request = request.body(bodyContentType, args.getBodyArg());
        }

        return request;
    }

    public Method getRequestMethod(ClientRequest request) throws NoSuchMethodException {
        String requestMethodName = GET;
        if (!isEmpty(args.getMethodArg())) {
            requestMethodName = args.getMethodArg();
        }

        return request.getClass().getMethod(requestMethodName.toLowerCase());
    }

    @ServiceCommand(defaultServiceClass=InvokeRestfulEndpointService.class)
    public interface InvokeRestfulEndpointCommand extends Command, InvokeRestfulEndpointArgument {
    }

    @ServiceArgument
    public interface InvokeRestfulEndpointArgument extends Argument {

        /*
         * The URL.
         * Required.
         */
        String getUrlArg();

        void setUrlArg(String urlArg);

        /*
         * Case insensitive HTTP method to use, e.g. GET, pOsT. Optional, defaults to
         * GET
         */
        String getMethodArg();

        void setMethodArg(String methodArg);

        /*
         * Map of header name/value pairs. This might include the Authorization and Accepts headers for example.
         * Optional.
         */
        Map<String, String> getHeadersArg();

        void setHeadersArg(Map<String, String> headersArg);

        /*
         * Map of query parameter name/value pairs.
         * Optional.
         */
        Map<String, String> getQueryParametersArg();

        void setQueryParametersArg(Map<String, String> queryParametersArg);

        /*
         * The body of the request.
         * Optional.
         */
        String getBodyArg();

        void setBodyArg(String bodyArg);

        /*
         * What to set for the body content type.
         * Optional, defaults to "application/json"
         */
        String getBodyContentTypeArg();

        void setBodyContentTypeArg(String bodyContentTypeArg);

        /*
         * What to set for the Authorization header
         * Optional.
         */
        String getAuthorizationTokenArg();

        void setAuthorizationTokenArg(String authorizationTokenArg);

        /*
         * The response from the call
         */
        Object getResponseRet();

        void setResponseRet(Object responseRet);
    }
}
