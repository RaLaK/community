/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.core.audit;

import static com.ail.core.audit.RevisionType.DELETE;
import static com.ail.core.audit.RevisionType.INSERT;
import static com.ail.core.audit.RevisionType.UPDATE;
import static org.hibernate.envers.RevisionType.ADD;
import static org.hibernate.envers.RevisionType.DEL;
import static org.hibernate.envers.RevisionType.MOD;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.envers.RevisionType;

import com.ail.core.Type;

public class Revision {

    private RevisionDetails revisionDetails;
    private Map<com.ail.core.audit.RevisionType, List<Type>> changes = new HashMap<>();

    public Revision(RevisionDetails revision,  Map<RevisionType, List<Type>> changes) {
        this.revisionDetails = revision;

        if (changes.get(ADD).size() > 0) {
            this.changes.put(INSERT, changes.get(ADD));
        }

        if (changes.get(MOD).size() > 0) {
            this.changes.put(UPDATE, changes.get(MOD));
        }

        if (changes.get(DEL).size() > 0) {
            this.changes.put(DELETE, changes.get(DEL));
        }
    }

    public Map<com.ail.core.audit.RevisionType, List<Type>> getChanges() {
        return changes;
    }

    public RevisionDetails getRevisionDetails() {
        return revisionDetails;
    }
}
