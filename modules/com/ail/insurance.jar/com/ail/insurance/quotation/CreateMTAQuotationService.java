/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.insurance.quotation;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.APPLIED;
import static com.ail.insurance.policy.PolicyStatus.NOT_TAKEN_UP;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.PostconditionException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.quotation.CreateMTAQuotationService.CreateMTAQuotationArgument;
import com.ail.insurance.quotation.InitialiseMTAService.InitialiseMTACommand;
import com.ail.insurance.quotation.PrepareRequoteService.PrepareRequoteCommand;

/**
 */
@ServiceImplementation
public class CreateMTAQuotationService extends Service<CreateMTAQuotationArgument> {
    private static final long serialVersionUID = 3819563603833694389L;

    @ServiceArgument
    public interface CreateMTAQuotationArgument extends Argument {
        void setPolicyArg(Policy policy);

        Policy getPolicyArg();

        void setMTARet(Policy mtaRet);

        Policy getMTARet();
    }

    @ServiceCommand(defaultServiceClass = CreateMTAQuotationService.class)
    public interface CreateMTAQuotationCommand extends Command, CreateMTAQuotationArgument {
    }

    @Override
    public void invoke() throws BaseException {
        if (args.getPolicyArg() == null) {
            throw new PreconditionException("args.getPolicyArg() == null");
        }

        if (args.getPolicyArg().getStatus() != ON_RISK) {
            throw new PreconditionException("args.getPolicyArg().getStatus() != ON_RISK");
        }

        markExistingMTAQuotationsAsNotTakenUp();

        createMTAQuotation();

        getCore().flush(); // flush the MTA so we get IDs etc. populated.

        linkMTAQuotationToPolicy();

        setMTAStatusToApplication();

        initialiseMTAQuotation();

        if (args.getMTARet() == null) {
            throw new PostconditionException("args.getMTARet() == null");
        }
    }

    private void setMTAStatusToApplication() {
        args.getMTARet().setStatus(APPLICATION);
    }

    private void initialiseMTAQuotation() throws BaseException {
        InitialiseMTACommand irc = getCoreProxy().newCommand("InitialiseMTA", InitialiseMTACommand.class);
        irc.setPolicyArg(args.getPolicyArg());
        irc.setMTAQuotationArg(args.getMTARet());
        irc.invoke();
    }

    private void linkMTAQuotationToPolicy() {
        args.getPolicyArg().getPolicyLink().add(new PolicyLink(MTA_QUOTATION_FOR, args.getMTARet().getSystemId()));
        args.getMTARet().getPolicyLink().add(new PolicyLink(MTA_QUOTATION_FROM, args.getPolicyArg().getSystemId()));
    }

    private void createMTAQuotation() throws BaseException {
        PrepareRequoteCommand prc = getCoreProxy().newCommand(PrepareRequoteCommand.class);
        prc.setPolicyArg(args.getPolicyArg());
        prc.setSuppressDocumentCopyArg(true);
        prc.invoke();

        args.setMTARet(getCoreProxy().create(prc.getRequoteRet()));
    }

    /**
     * Search for all quotations that were created as MTAs for this policy in the past.
     * Any that are found will which have not been applied are marked as NOT_TAKEN_UP.
     */
    private void markExistingMTAQuotationsAsNotTakenUp() {
        args.getPolicyArg().getPolicyLink().
                stream().
                filter(pl -> pl.getLinkType() == MTA_QUOTATION_FOR).
                forEach(pl -> {
                    Policy oldRenewal = (Policy)getCoreProxy().queryUnique("get.policy.by.systemId", pl.getTargetPolicyId());
                    if (oldRenewal.getStatus() != APPLIED && oldRenewal.getStatus() != SUBMITTED) {
                        oldRenewal.setStatus(NOT_TAKEN_UP);
                    }
                });
    }
}
