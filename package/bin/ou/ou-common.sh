#!/bin/bash

function json-query {
	node -pe "JSON.parse(fs.readFileSync(0)).$1" 2> /dev/null
}

function usage {
	echo "ou: $1"
	echo "usage: <function-group> <function> [arguments]"
	exit 1
}

function error {
	echo "ou error: $1"
	echo "usage: <function-group> <function> [arguments]"
	exit 1
}

function warning {
	echo "ou warning: $1" 
}

function info {
	echo "ou info: $1" 
}

function load-cloudflare-env {
	[ -f ~/.cloudflare ] && . ~/.cloudflare

	[ -z "$CLOUDFLARE_ZONE_ID" ] && usage "CLOUDFLARE_ZONE_ID is not defined (or in ~/.cloudflare)" 
	[ -z "$CLOUDFLARE_USER" ] && usage "CLOUDFLARE_USER is not defined (or in ~/.cloudflare)"
	[ -z "$CLOUDFLARE_API_KEY" ] && usage "CLOUDFLARE_API_KEY is not defined (or in ~/.cloudflare)"
}

function debug-on {
	[ "$ARG_debug" == "yes" ] && set -x
	return 0
}

function debug-off {
	[ "$ARG_debug" == "yes" ] && set -
	return 0
}

function branch-name {
	[ -z "$ARG_branch" ] && usage "-branch=<branch name> not specified"
	echo $ARG_branch
}

function exit-on-error {
	trap "echo 'Exiting due to previous error.'; exit 1;" ERR
}
