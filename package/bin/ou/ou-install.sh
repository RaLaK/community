#!/bin/bash

function help {
	echo "aws-toolkit"	
	echo "newman"
}

function aws-toolkit {
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python get-pip.py
    pip install awscli --upgrade
}

function newman {
    curl -sL https://deb.nodesource.com/setup_6.x | bash -
    apt-get install -y nodejs
    npm install -g newman
}

function dnsutils {
	apt-get update
	apt-get install -y dnsutils
}

