package com.ail.insurance;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import org.junit.Test;

import com.ail.party.Address;

public class FunctionsTest {


    @Test
    public void testRemoveHtmlTags() {
        String text = "<p>text</p><p>text</p><p>text</p>";
        assertEquals("texttexttext", Functions.removeHtmlTags(text));
    }

    @Test
    public void testFormatAddressWithNullAddress() {
        assertThat(Functions.formatAddress(null, false), is(""));
    }

    @Test
    public void testFormatAddressWithNullAddressElements() {
        Address address = mock(Address.class);

        doReturn("address line 1").when(address).getLine1();
        doReturn("address line 2").when(address).getLine2();
        doReturn("postcode").when(address).getPostcode();

        assertThat(Functions.formatAddress(address, true), is("address line 1<br/>address line 2<br/>postcode"));
    }
}
