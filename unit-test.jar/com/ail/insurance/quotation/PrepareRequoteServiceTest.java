package com.ail.insurance.quotation;

import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static java.util.Arrays.asList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.Core;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.ExceptionRecord;
import com.ail.core.Note;
import com.ail.core.PreconditionException;
import com.ail.core.XMLString;
import com.ail.core.document.Document;
import com.ail.insurance.claim.Claim;
import com.ail.insurance.policy.AssessmentLine;
import com.ail.insurance.policy.AssessmentSheet;
import com.ail.insurance.policy.Coverage;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.Section;
import com.ail.insurance.quotation.PrepareRequoteService.PrepareRequoteArgument;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CoreContext.class)
public class PrepareRequoteServiceTest {

    private static final String PRODUCT_NAME = "product name";

    PrepareRequoteService sut;

    @Mock
    private PrepareRequoteArgument args;
    @Mock
    private Policy policy;
    @Mock
    private Policy requote;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private Core core;
    @Mock
    private AssessmentSheet policyAassessmentSheet;
    @Mock
    private AssessmentSheet sectionAassessmentSheet;
    @Mock
    private Map<String,AssessmentLine> policyAssessmentSheetLinesMap;
    @Mock
    private Map<String,AssessmentLine> sectionAssessmentSheetLinesMap;
    @Mock
    private Section section;
    @Mock
    private XMLString xmlString;
    @Mock
    private Policy donorPolicy;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockStatic(CoreContext.class);

        sut = new PrepareRequoteService();

        sut.setArgs(args);

        doReturn(policy).when(args).getPolicyArg();
        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);
        when(CoreContext.getProductName()).thenReturn(PRODUCT_NAME);
        doReturn(PRODUCT_NAME).when(policy).getProductTypeId();
        doReturn(requote).when(coreProxy).newProductType(eq(PRODUCT_NAME), eq("Policy"), eq(Policy.class));
        doReturn(core).when(coreProxy).getCore();
        doReturn(policyAassessmentSheet).when(requote).getAssessmentSheet();
        doReturn(policyAssessmentSheetLinesMap).when(policyAassessmentSheet).getAssessmentLine();
        doReturn(asList(section)).when(requote).getSection();
        doReturn(sectionAassessmentSheet).when(section).getAssessmentSheet();
        doReturn(sectionAssessmentSheetLinesMap).when(sectionAassessmentSheet).getAssessmentLine();
        doReturn(xmlString).when(coreProxy).toXML(eq(policy));
        doReturn(donorPolicy).when(coreProxy).fromXML(eq(Policy.class), eq(xmlString));
    }

    @Test(expected=PreconditionException.class)
    public void checkThatNullPolicyArgIsTrapped() throws Exception {
        doReturn(null).when(args).getPolicyArg();

        sut.invoke();
    }

    @Test
    public void checkThatDataIsMergedFromPolicyIntoRequote() throws Exception {
        sut.invoke();
        verify(requote).mergeWithDataFrom(eq(donorPolicy), eq(core));
    }

    @Test
    public void checkThatRequoteStatusIsSetToApplication() throws Exception {
        sut.invoke();
        verify(requote).setStatus(eq(APPLICATION));
    }

    @Test
    public void checkThatRequoteUserSavedFlagIsCleared() throws Exception {
        sut.invoke();
        verify(requote).setUserSaved(eq(false));
    }

    @Test
    public void checkThatRequoteIsMarkedAsNotPersisted() throws Exception {
        sut.invoke();
        verify(requote).markAsNotPersisted();
    }

    @Test
    public void checkThatRequoteAssessmentSheetLinesIsCleared() throws Exception {
        sut.invoke();
        verify(policyAssessmentSheetLinesMap).clear();
        verify(sectionAssessmentSheetLinesMap).clear();
    }

    @Test
    public void checkDocumentsAreCopiedByDefault() throws PreconditionException, BaseException, CloneNotSupportedException {
        Document currentDocument = mock(Document.class);
        Document archiveDocument = mock(Document.class);

        doReturn(currentDocument).when(currentDocument).clone();
        doReturn(archiveDocument).when(archiveDocument).clone();

        doReturn(asList(currentDocument)).when(policy).getDocument();
        doReturn(asList(archiveDocument)).when(policy).getArchivedDocument();

        doReturn(mock(ArrayList.class)).when(requote).getDocument();
        doReturn(mock(ArrayList.class)).when(requote).getArchivedDocument();

        sut.invoke();

        verify(requote.getDocument()).clear();
        verify(requote.getArchivedDocument()).clear();

        verify(requote.getDocument()).add(eq(currentDocument));
    }

    @Test
    public void checkDocumentsAreNotCopiedIfSuppressArgIsTrue() throws PreconditionException, BaseException, CloneNotSupportedException {
        Document currentDocument = mock(Document.class);
        Document archiveDocument = mock(Document.class);

        doReturn(asList(currentDocument)).when(policy).getDocument();
        doReturn(asList(archiveDocument)).when(policy).getArchivedDocument();

        doReturn(mock(ArrayList.class)).when(requote).getDocument();
        doReturn(mock(ArrayList.class)).when(requote).getArchivedDocument();

        doReturn(true).when(args).getSuppressDocumentCopyArg();

        sut.invoke();

        verify(requote.getDocument()).clear();
        verify(requote.getArchivedDocument()).clear();

        verify(requote.getDocument(), never()).add(eq(currentDocument));
    }

    @Test
    public void checkNotesAreNotCopied() throws BaseException {
        Note policyNote = mock(Note.class);

        doReturn(asList(policyNote)).when(policy).getNote();

        doReturn(mock(ArrayList.class)).when(requote).getNote();

        sut.invoke();

        verify(requote.getNote()).clear();
    }

    @Test
    public void checkClaimsAreNotCopied() throws BaseException {
        Claim claim = mock(Claim.class);

        doReturn(asList(claim)).when(policy).getClaim();
        doReturn(mock(ArrayList.class)).when(requote).getClaim();

        sut.invoke();

        verify(requote.getClaim()).clear();
    }

    @Test
    public void checkExceptonsAreNotCopied() throws BaseException {
        ExceptionRecord exceptionRecord = mock(ExceptionRecord.class);

        doReturn(asList(exceptionRecord)).when(policy).getException();
        doReturn(mock(ArrayList.class)).when(requote).getException();

        sut.invoke();

        verify(requote.getException()).clear();
    }

    @Test
    public void checkCoveragesAreCopied() throws BaseException, CloneNotSupportedException {
        Coverage policyCoverage = mock(Coverage.class);
        doReturn(asList(policyCoverage)).when(requote).getCoverage();

        Coverage sectionCoverage = mock(Coverage.class);
        doReturn(asList(sectionCoverage)).when(section).getCoverage();

        sut.invoke();

        verify(policyCoverage).markAsNotPersisted();
        verify(sectionCoverage).markAsNotPersisted();
    }
}
